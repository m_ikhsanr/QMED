/*
 * HISTORY VERSIONING
 * PROGRAMMER
 * INITIAL              VERSION         DATE(YYYY-MM-DD)        CHANGE DESCRIPTION
 * ========================================================================================================
 * CHRIS                1.0             2018-04-27              Push Notification support system and major code order & "title comment" as grouping
 * CHRIS                1.1             2018-05-02              - major function migration to BaseActivity
 *                                                              - checkAppUpdate. Show dialog when criteria met on prmeter_tp
 *                                                              - TODO: Handle when err true
 * CHRIS                1.2             2018-05-18              handle permission for SDK >= 23
 * IKHSAN               1.3             2018-05-24              - Membuat fungsi untuk request beberapa permission sekaligus
 *                                                              - Membuat fungsi setHeader() untuk memanggil fungsi2 prepare header, prepare webview dll
 *                                                              - Menonaktifkan button fisik Back, jika user menekan back akan menampilkan
 *                                                                Toast message
 *                                                              - Mengubah kondisi untuk menampilkan progress bar, dari API 25 (Nougat) menjadi 23 (Marshmallow)
 *                                                              - Handle permissions jika "Allow" atau "Denied" dengan fungsi onRequestPermissionsResult
 *                                                              - Handle permissions untuk penggunaan kamera dan storage saat user akan upload foto
 *                                                              - Dialog penjelasan jika permission ditolak
 * IKHSAN               1.4             2018-05-28              - Handle bug swipe refresh saat scroll halaman dan crop foto
 *                                                              - Handle network status
 *                                                              - Memperbaiki urutan create webview pada onCreate
 *
 */

package com.mdkit.qmed;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends BaseActivity
        implements ConnectivityReceiver.ConnectivityReceiverListener {

    // permissions (added as SDK is more than 23. Below 23, it is enough to declare this in manifest file)
    // https://stackoverflow.com/questions/32083913/android-gps-requires-access-fine-location-error-even-though-my-manifest-file?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    // solved following error : java.lang.SecurityException: "varname" location provider requires ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION permission.
    // V 1.2 [S]
    private static final String CLOC_PMSION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String FLOC_PMSION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String CAM_PMSION = Manifest.permission.CAMERA;
    // V 1.3 [S]
    private static final String RESTO_PMSION = Manifest.permission.READ_EXTERNAL_STORAGE;
    private static final String WRSTO_PMSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final int RESTO_PMSION_REQCODE = 13;
    private static final int WRSTO_PMSION_REQCODE = 14;
    private static final int FILE_PMSION_REQCODE = 15;
    // V 1.3 [E]
    // https://stackoverflow.com/questions/33331073/android-what-to-choose-for-requestcode-values?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    private static final int CLOC_PMSION_REQCODE = 10;
    private static final int FLOC_PMSION_REQCODE = 11;
    private static final int CAM_PMSION_REQCODE = 12;
    // V 1.2 [E]
    private static final String TAG = MainActivity.class.getSimpleName();
    private ValueCallback<Uri> mUploadMessage;
    private Uri mCapturedImageURI = null;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    private static final int INPUT_FILE_REQUEST_CODE = 1;
    private static final int FILECHOOSER_RESULTCODE = 1;
    private String webURL;
    SwipeRefreshLayout swipeRefreshLayout;
    WebView mWebView;
    double latitude, longitude;
    String loclonglatt = "", locaddress = "", loccity = "";
    String prmtr = "";
    String code = "";
    String baseURL = "";
    String insertURL = "";
    ProgressDialog progressDialog;
    FirebaseDatabase firebaseDatabase;
    ProgressBar progressBar;
    DatabaseReference reference;
    Map<String, String> map = new HashMap<>();
    // V 1.0 [S]
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView txtMessage;
    // V 1.0 [E]
    boolean err = false; // V 1.1
    // V 1.3 [S]
    private Uri[] imageUri;
    private File photoFile = null;
    private String img64 = "";
    private Uri[] results = null;
    // V 1.3 [E]

    /** ======================================================================================
        ==================== MAIN OVERRIDE FUNCTIONS + THE WEB CLIENT ========================
        ======================================================================================
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "##### ===================== START onCreate =====================");
        err = false;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkConnection(); // 1.4 [S.E]

        // V 1.2 [S]
//        checkPermissions(CLOC_PMSION, CLOC_PMSION_REQCODE);
//        checkPermissions(FLOC_PMSION, FLOC_PMSION_REQCODE);
//        checkPermissions(CAM_PMSION, CAM_PMSION_REQCODE);
        // V 1.2 [E]

        // V 1.3 [S]
        permissionsCheck(CLOC_PMSION, CLOC_PMSION_REQCODE);
        permissionsCheck(FLOC_PMSION, FLOC_PMSION_REQCODE);
        // V 1.3 [E]

        // V 1.0 [S]
        /** ====================================================================
        * A. Prepare push notification
        */
        txtMessage = (TextView) findViewById(R.id.txt_push_message);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "##### START onReceive -ing notification from firebase...");
                if (intent.getAction().equals(ConfigCustom.PUSH_NOTIF_INTENT)) {
                    String message = intent.getStringExtra(getString(R.string.pnotif_data_key02));
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    txtMessage.setText(message);
                }
                Log.d(TAG, "##### END onReceive -ing notification from firebase");
            }
        };
        // V 1.0 [E]

        /** ====================================================================
         * B. Prepare main web view
         */
//        prmtr = getString(R.string.prmtr);
//        code = getString(R.string.code);

//        final Map<String, String> map = new HashMap<>();
//        map.put(prmtr, code);

        mWebView = (WebView) findViewById(R.id.webView);

//        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference("dontelpoint_URL");
//        mDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                String value = dataSnapshot.getValue(String.class);
//                String[] values = value.split(",");
//                BASE_URL = values[0].replace("\"", "");
//                mWebView.loadUrl( BASE_URL , map);
//                getLocationInfo(values[1].replace("\"", ""));
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                Toast.makeText(MainActivity.this, "Gagal memuat data. Silahkan tutup dan buka kembali aplikasi.", Toast.LENGTH_LONG).show();
//            }
//        });

        /** ---------------------------------------------------------------------
         * Prepare main web view -> URL + header
         * This cant be combined with in_did.php because :
         * - both are running in different thread
         * - trigger is different; in_did.php is called onTokenChange and ge_heax.php is called when header is requested
         *
         */
//        firebaseDatabase = FirebaseDatabase.getInstance();
//        reference = firebaseDatabase.getReference("donatello_poin");
//        reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Link link = dataSnapshot.getValue(Link.class);
//                prmtr      = link.getDontelpoint_header_prmeter();
//                code       = link.getDontelpoint_header_code();
//                // TESTING [S]
////                baseURL   = link.getDontelpoint_URL();
//                baseURL = link.getDontellpoint_URL_dev();
//                // TESTING [E]
//                insertURL = link.getDontellpoint_URL_insert();
//
//                map.put(prmtr, code);
//                mWebView.loadUrl(baseURL, map);
//                getLocationInfo(insertURL);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });


        // V 1.3 [S]
//        try {
//
//                // 1. prepare header to access the web properly
//                Log.d(TAG, "##### START preparing header...");
//                HashMap rqHeader = prepHeaderRequest();
//                if (rqHeader == null) {
//                    err = true;
//                }
//                Log.d(TAG, "##### END preparing header...");
//
//                // 2. send request with the prepared header
//                Log.d(TAG, "##### START async SendToAppServer...");
//                // 2. - alt 1
//                SendToAppServer req = new SendToAppServer();
////            req.execute(rqArgs);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                    req.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, rqHeader);
//                } else {
//                    req.execute(rqHeader);
//                }
//                String resp = req.get(); //this line break the purpose of asynctask but having the process relatively simple (only text - short running task), it is still suitable for this purpose. Please refer to version 2 to fully utilize async feature
//                if (resp == null) {
//                    err = true;
//                }
//
//                // 2. - alt 2
////            BaseTask bt = new BaseTask();
////            bt.launchTask(rqArgs);
//                Log.d(TAG, "##### END async SendToAppServer. resp : " + resp);
//
//                // 3. Handle response : check app update and load the URL
//                Log.d(TAG, "##### START handling response (check app update and load url) ...");
//                String[] rpArr = resp.split(",");
//
//                baseURL = rpArr[2];
//                map.put(rpArr[0].toString(), rpArr[1].toString()); // 0: parameter name , 1: parameter value
//
//                checkAppUpdate(rpArr[3].toString(), rpArr[4].toString()); // V 1.1
//
//                mWebView.loadUrl(baseURL, map);
//                Log.d(TAG, "##### END handling response...");
//
//
//        }catch(Exception e){
//            e.printStackTrace();
//        }

        if( permissionsIsGranted(CLOC_PMSION) &&
                permissionsIsGranted(FLOC_PMSION) ) {
            setHeader();
        }
        // V 1.3 [E]

        /** ---------------------------------------------------------------------
         * B. Prepare main web view -> progress bar + the web view
         */
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Memuat data..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                webURL = mWebView.getUrl();
//                mWebView.loadUrl(webURL);
//                loadWeb();
                // 1.3 [S]
//                if (Build.VERSION.SDK_INT < 25) {
                if (Build.VERSION.SDK_INT <= 23) {
                // 1.3 [E]
                    progressDialog.show();
                // 1.3 [S]
//                } else if (Build.VERSION.SDK_INT >= 25) {
                } else if (Build.VERSION.SDK_INT > 23) {
                // 1.3 [E]
                    progressBar.setVisibility(View.VISIBLE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

//                webURL = mWebView.getUrl();
//                if (webURL.contains("index.php") || webURL.equals(baseURL) || webURL.contains("article")) {
//                    mWebView.loadUrl(webURL, map);
//                } else if ( webURL.contains("404.php") ) {
//                    mWebView.loadUrl(baseURL, map);
//                } else {
//                    mWebView.loadUrl(webURL);
//                }

                webURL = mWebView.getUrl();
                mWebView.loadUrl(webURL);
//                loadWeb();
            }
        });

//        mWebView.loadUrl(baseURL, map); // 1.4 [S.E]
//        loadWeb();
        // V 1.3 [S]
        mWebView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public String getImg64(){
                img64 = getB64Img();
                return img64;
            }
        }, "img_native");
        // V 1.3 [E]

        // 1.4 [S]
        mWebView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void setSwipeRefreshOn(){
                swipeRefreshLayout.setEnabled(true);
            }
        }, "native_swipeOn");

        mWebView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void setSwipeRefreshOff(){
                swipeRefreshLayout.setEnabled(false);
            }
        }, "native_swipeOff");

        // 1.4 [E]

        // V 1.0 [S]
        /** ---------------------------------------------------------------------
         * B. Prepare main web view -> javascript
         */
        mWebView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public String readFbToken(){
                return readFirebaseToken();
            }
        }, "native");
        // V 1.0 [E]

        // V 1.4 [S]
        loadWeb();
        mWebView.setWebChromeClient(new ChromeClient());
        mWebView.loadUrl(baseURL, map);
        // V 1.4 [E]

        Log.d(TAG, "##### ===================== END onCreate =====================");
    }

    // 1.3 [S]
    public void setHeader() {
        try {
//          // 1. prepare header to access the web properly
            Log.d(TAG, "##### START preparing header...");
            HashMap rqHeader = prepHeaderRequest();
            if (rqHeader == null) {
                err = true;
            }
            Log.d(TAG, "##### END preparing header...");

            // 2. send request with the prepared header
            Log.d(TAG, "##### START async SendToAppServer...");
            // 2. - alt 1
            SendToAppServer req = new SendToAppServer();
//            req.execute(rqArgs);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                req.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, rqHeader);
            } else {
                req.execute(rqHeader);
            }
            String resp = req.get(); //this line break the purpose of asynctask but having the process relatively simple (only text - short running task), it is still suitable for this purpose. Please refer to version 2 to fully utilize async feature
            if (resp == null) {
                err = true;
            }

            // 2. - alt 2
//            BaseTask bt = new BaseTask();
//            bt.launchTask(rqArgs);
            Log.d(TAG, "##### END async SendToAppServer. resp : " + resp);

            // 3. Handle response : check app update and load the URL
            Log.d(TAG, "##### START handling response (check app update and load url) ...");
            String[] rpArr = resp.split(",");

            baseURL = rpArr[2];
            map.put(rpArr[0].toString(), rpArr[1].toString()); // 0: parameter name , 1: parameter value

            checkAppUpdate(rpArr[3].toString(), rpArr[4].toString()); // V 1.1

            // for testing only
//            baseURL = "http://qmed.co.id/web";
//            baseURL = "http://192.168.100.5/qmed/web/app_dev.php";
//            map.put("X-MDK-EXCLUSIVE", "f7j33gpw8w0v041wy2c1");

            Log.d(TAG, "##### END handling response...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // V 1.3 [E]

    // V 1.0 [S]
    @Override
    protected void onResume() {
        Log.d(TAG, "##### START onResume");
        super.onResume();

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ConfigCustom.PUSH_NOTIF_INTENT));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());

        // 1.4 [S]
        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
        // 1.4 [E]

        Log.d(TAG, "##### END onResume");
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        webURL = mWebView.getUrl();
//        if (webURL.contains("profile")) {
//            mWebView.loadUrl(webURL);
//        }
//    }

    @Override
    protected void onPause() {
        Log.d(TAG, "##### START onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
        Log.d(TAG, "##### END onPause");
    }
    // V 1.0 [E]

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "##### START onBackPressed");
        // V 1.3 [S]
        String url = mWebView.getUrl();
        if (url.contains("homepage") || url.contains("login") || url.contains("dashboard")) {
            moveTaskToBack(true);
        } else {
            Toast.makeText(this, R.string.app_backPressed_msg, Toast.LENGTH_SHORT).show();
        }
        // V 1.3 [E]
        Log.d(TAG, "##### END onBackPressed");
    }

//    @Override
//    public void onBackPressed() {
//        WebView myWebView = (WebView)findViewById(R.id.webView);
//        if(myWebView.getUrl().contains("profile") || myWebView.getUrl().contains("register")
//                || myWebView.getUrl().contains("index") || myWebView.getUrl().contains("article")) {
//            moveTaskToBack(true);
//        } else {
//            if(myWebView.canGoBack()) {
//                myWebView.goBack();
//            } else {
//                moveTaskToBack(true);
//            }
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            // Uri[] results = null; V 1.3 [S.E]

            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    // If there is not data, then we may have taken a photo
//                    img64 = getB64Img();
                    if (mCameraPhotoPath != null) {
                        // V 1.3 [S]
                        results = imageUri;
//                        results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                        // V 1.3 [E]
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }
            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;

        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }

            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == this.mUploadMessage) {
                    return;
                }
                Uri result = null;
                try {
                    if (resultCode != RESULT_OK) {
                        result = null;
                    } else {
                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {

                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
        return;
    }

    public void loadWeb() {
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setAllowContentAccess(true);

        // 1.4 [S]
//        mWebView.getSettings().supportZoom();
//        mWebView.getSettings().getBuiltInZoomControls();
//        mWebView.getSettings().setAppCacheMaxSize(1024 * 1024 * 5);
//        mWebView.getSettings().setLoadWithOverviewMode(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        // 1.4 [E]

        swipeRefreshLayout.setRefreshing(true);

        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                swipeRefreshLayout.setRefreshing(true);
                // 1.3 [S]
//                if (Build.VERSION.SDK_INT < 25) {
                if (Build.VERSION.SDK_INT <= 23) {
                // 1.3 [E]
                    progressDialog.show();
                // 1.3 [S]
//                } else if (Build.VERSION.SDK_INT >= 25) {
                } else if (Build.VERSION.SDK_INT > 23) {
                // 1.3 [E]
                    progressBar.setVisibility(View.VISIBLE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
                mWebView.loadUrl(url);
                return false;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                swipeRefreshLayout.setRefreshing(true);
                // 1.3 [S]
//                if (Build.VERSION.SDK_INT < 25) {
                if (Build.VERSION.SDK_INT <= 23) {
                // 1.3 [E]
                    progressDialog.show();
                // 1.3 [S]
//                } else if (Build.VERSION.SDK_INT >= 25) {
                } else if (Build.VERSION.SDK_INT > 23) {
                // 1.3 [E]
                    progressBar.setVisibility(View.VISIBLE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
                url = mWebView.getUrl();

                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                swipeRefreshLayout.setRefreshing(false);
                // 1.3 [S]
//                if (Build.VERSION.SDK_INT < 25) {
                if (Build.VERSION.SDK_INT <= 23) {
                // 1.3 [E]
                    progressDialog.dismiss();
                // 1.3 [S]
//                } else if (Build.VERSION.SDK_INT >= 25) {
                } else if (Build.VERSION.SDK_INT > 23) {
                // 1.3 [E]
                    progressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                // V 1.4 [S]

                // call javascript to pass firebase device id token
                String js_string = "javascript:" +
                        "function getFbToken(){" +
                            "return native.readFbToken();" +
                        "};";

                mWebView.loadUrl(js_string);

                // V 1.3 [S]
                String js_image_string = "javascript:" +
                        "function jsGetImg64(){" +
                        "return img_native.getImg64();" +
                        "};";
                mWebView.loadUrl(js_image_string);
                // V 1.3 [E]

                // V 1.4 [S]
//                String jsSwipeOn = "javascript:" +
//                        "function setSwipeOn() { native_swipeOn.setSwipeRefreshOn(); };";
//                mWebView.loadUrl(jsSwipeOn);
//
//                String jsSwipeOff = "javascript:" +
//                        "function setSwipeOff() { native_swipeOff.setSwipeRefreshOff(); };";
//                mWebView.loadUrl(jsSwipeOff);

                // V 1.4 [E]

                // V 1.4 [E]
            }

        });
    }

    public class ChromeClient extends WebChromeClient {

        // For Android 5.0 above
        @Override
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {

            // Double check that we don't have any existing callbacks
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePath;

            // V 1.3 [S]
            String[] PERMISSIONS = {CAM_PMSION, WRSTO_PMSION, RESTO_PMSION};

            if(!multiPermissionsRequest(MainActivity.this, PERMISSIONS)){
                requestPermissions(PERMISSIONS, FILE_PMSION_REQCODE);
            }

            if ( permissionsIsGranted(WRSTO_PMSION) &&
                    permissionsIsGranted(RESTO_PMSION) &&
                    permissionsIsGranted(CAM_PMSION) ) {
            // V 1.3 [E]

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
//                File photoFile = null; // 1.3 [S.E]
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                    } catch (Exception ex) {
                        // Error occurred while creating the File
                        ex.printStackTrace();
                    }

                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                        imageUri = new Uri[]{Uri.fromFile(photoFile)};

                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }

                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("image/*");
                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }
                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Pilih Foto Dari :");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);

            // V 1.3 [S]
            } else {

//                String[] PERMISSIONS = {CAM_PMSION, WRSTO_PMSION, RESTO_PMSION};

                if(!multiPermissionsRequest(MainActivity.this, PERMISSIONS)){
                    requestPermissions(PERMISSIONS, FILE_PMSION_REQCODE);
                }
            }
            // V 1.3 [E]
            return true;

        }

        // openFileChooser for Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {

            mUploadMessage = uploadMsg;
            File imageStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES)
                    , "AndroidExampleFolder");
            if (!imageStorageDir.exists()) {
                imageStorageDir.mkdirs();
            }
            File file = new File(
                    imageStorageDir + File.separator + "IMG_"
                            + String.valueOf(System.currentTimeMillis())
                            + ".jpg");

            mCapturedImageURI = Uri.fromFile(file);
            final Intent captureIntent = new Intent(
                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            Intent chooserIntent = Intent.createChooser(i, "Pilih Foto Dari :");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                    , new Parcelable[] { captureIntent });
            startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
        }

//        // openFileChooser for Android < 3.0
//        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
//            openFileChooser(uploadMsg, "");
//        }
//
//        //openFileChooser for other Android versions
//        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
//            openFileChooser(uploadMsg, acceptType);
//        }

    }

    // V 1.2 [S]
//    @TargetApi(Build.VERSION_CODES.M) // API 23
//    public boolean checkPermissions(String pmsion, int reqCode) {
//        int permissionCheckRead = ContextCompat.checkSelfPermission(this, pmsion);
//        if (permissionCheckRead != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) this, pmsion)) {
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//                ActivityCompat.requestPermissions((Activity) this, new String[]{pmsion}, reqCode);
//            } else {
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions((Activity) this, new String[]{pmsion}, reqCode);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//            return false;
//        } else
//            return true;
//    }
    // V 1.2 [E]

    // V 1.3 [S]
    // PERMISSIONS
    public static boolean multiPermissionsRequest(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void permissionsCheck(String permission_name, int permission_code) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(!permissionsIsGranted(permission_name)) {
                requestPermissions(permission_name, permission_code);
            }
        } else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if(!permissionsIsGranted(permission_name)) {
                requestPermissions(permission_name, permission_code);
            }
        }
    }

    private boolean permissionsIsGranted(String permission_name){
        return ( ContextCompat.checkSelfPermission(MainActivity.this, permission_name) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermissions(String permission_name, int permission_code) {
        ActivityCompat.requestPermissions(this, new String[]{permission_name}, permission_code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permission[], @NonNull int grantResults[]){
        String locMsg = getString(R.string.app_askLoc_msg);
        String camMsg = getString(R.string.app_askCam_msg);
        String stoMsg = getString(R.string.app_askSto_msg);

        switch (requestCode){
            case CLOC_PMSION_REQCODE :
                if (grantResults.length > 0) {
                    boolean perAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(!perAccepted){
                        // jika permissions ditolak
                        // akan menampilkan dialog penjelasan
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if(shouldShowRequestPermissionRationale(CLOC_PMSION)) {
                                displayAlertMessage(locMsg,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[] {CLOC_PMSION}, CLOC_PMSION_REQCODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    } else {
                        // jika permissions diterima
                        // akan menjalankan setHeader(), memanggil prepare header, send header, dll
                        setHeader();
                        return;
                    }
                }
                break;
            case FLOC_PMSION_REQCODE :
                if (grantResults.length > 0) {
                    boolean perAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(!perAccepted){
                        // jika permissions ditolak
                        // akan menampilkan dialog penjelasan
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if(shouldShowRequestPermissionRationale(FLOC_PMSION)) {
                                displayAlertMessage(locMsg,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[] {FLOC_PMSION}, FLOC_PMSION_REQCODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    } else {
                        // jika permissions diterima
                        // akan menjalankan setHeader(), memanggil prepare header, send header, dll
                        setHeader();
                        return;
                    }
                }
                break;
            case FILE_PMSION_REQCODE :
                if (grantResults.length > 0) {
                    boolean perAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(!perAccepted){
                        // jika permissions ditolak
                        // akan menampilkan dialog penjelasan
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if(shouldShowRequestPermissionRationale(CAMERA)) {
                                displayAlertMessage(camMsg,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[] {CAMERA}, FILE_PMSION_REQCODE);
                                                }
                                            }
                                        });
                                return;
                            }
                            if(shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                displayAlertMessage(stoMsg,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[] {WRITE_EXTERNAL_STORAGE}, FILE_PMSION_REQCODE);
                                                }
                                            }
                                        });
                                return;
                            }
                            if(shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                                displayAlertMessage(stoMsg,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[] {READ_EXTERNAL_STORAGE}, FILE_PMSION_REQCODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    } else {
                        // jika permissions diterima
                        // akan menampilkan "File Chooser" atau "Pilih File Dari"
                        // yang menampilkan pilihan upload file dari "Camera" atau "File Explorer"
                        callFileChooser();
                    }
                }
                break;
        }
    }

    // dialog atau alert message penjelasan jika permission ditolak
    public void displayAlertMessage(String message, DialogInterface.OnClickListener listener ) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setNeutralButton("OK", listener)
                .create()
                .show();
    }

    private void callFileChooser() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
                takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                imageUri = new Uri[]{Uri.fromFile(photoFile)};

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
            } else {
                takePictureIntent = null;
            }
        }

        Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
        contentSelectionIntent.setType("image/*");
        Intent[] intentArray;
        if (takePictureIntent != null) {
            intentArray = new Intent[]{takePictureIntent};
        } else {
            intentArray = new Intent[0];
        }
        Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Pilih Foto Dari :");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);
    }

    // PHOTOS
    private String getB64Img()
    {
        FileInputStream fis = null;
        Bitmap bm = null;

        // this is how the arg should look like
        // fis = new FileInputStream("/storage/emulated/0/DCIM/Camera/IMG_20180201_153245.jpg");
        try{
            // means that results is from camera, starts with file://
            fis = new FileInputStream(this.results[0].toString().split("file://")[1]);
        }catch(ArrayIndexOutOfBoundsException e){
            try{
                // means that results is from File, starts with content://
                fis = new FileInputStream(getImagePath(this.results[0]));
            }catch(FileNotFoundException ex){
                e.printStackTrace();
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        bm = BitmapFactory.decodeStream(fis);


//        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,30,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;

    }

    public String getImagePath(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
    // V 1.3 [E]


    // 1.4 [S]
    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showNetworkToast(isConnected);
    }

    private void showNetworkToast(boolean isConnected) {
        String message;
        if (isConnected) {
            message = "Terhubung dengan internet.";
        } else {
            message = "Maaf, Anda tidak terhubung dengan internet.";
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }
//    handle Network
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNetworkToast(isConnected);
    }
    // 1.4 [E]

}
