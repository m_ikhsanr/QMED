package com.mdkit.qmed;


import android.content.Context;
import android.util.Log;

import com.mdkit.qmed.BaseTaskListener.BaseTaskListenerInterface;

import java.util.HashMap;

public class BaseTask implements BaseTaskListenerInterface<String> {
    private static final String TAG = BaseTask.class.getSimpleName();
    private Context context;

    public void onTaskComplete(String resp) {
        // do whatever you need
        Log.d(TAG, "##### START onTaskComplete. resp : "+resp);
        if(resp != null){
//            Map<String, String> map = new HashMap<>();
//            String[] rpArr = resp.split(",");
//
//            map.put(rpArr[0], rpArr[1]);
//            MainActivity ma = new MainActivity();
//            ma.mWebView.loadUrl(rpArr[2], map); <-- cant call this here
        }else{
            // oops... you are not allowed to use this app (header validation failed)
        }

        Log.d(TAG, "##### END onTaskComplete.");
    }

    public void launchTask(HashMap arg) {
        Log.d(TAG, "##### START launchTask.");
        SendToAppServer req = new SendToAppServer(context, this);
        req.execute(arg);
    }
}
