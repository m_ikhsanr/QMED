/*
 * HISTORY VERSIONING
 * PROGRAMMER
 * INITIAL              VERSION         DATE(YYYY-MM-DD)        CHANGE DESCRIPTION
 * ========================================================================================================
 * CHRIS                1.0             2018-04-27              initial version
 *
 */

/* IMPORTANT NOTES :

    1. Sample JSON Request :
        {
          "registration_ids" : ["dP3hRnJAZ3E:APA91bF1fkG8qjGPnOncvffPt2X9eEuRotGkdIzU7FbjJeQcq1Hraulr6gfuhlpJZgCKtjlJkIXTwq0T-fLsy6xk19M_VZ9ivTMkSnzKw6jt2S46TeW3zHYJMdi_PTsm58nwxjbjk7sx"],
          "data" : {
            "title":"YourTitle2",
            "message":"YourMessage2"
          }
        }

    2. NotificationChannel support for OREO. Must update development environment

    3.
 */

package com.mdkit.qmed;

//import android.app.NotificationChannel;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseMsgSvc extends FirebaseMessagingService {

    private static final String TAG = FirebaseMsgSvc.class.getSimpleName();
    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        Log.d(TAG, "##### From: " + remoteMessage.getFrom());
        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload. -- NOT USED. Always send with data payload
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "##### Notification payload: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "##### Data payload: " + remoteMessage.getData());

//            if (/* Check if data needs to be processed by long running job */ false) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "##### Exception: " + e.getMessage());
                e.printStackTrace();
            }
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        // This line below is to show notification from FCM to phone screen
//        sendNotification(remoteMessage.getNotification().getBody());
//        sendNotification(remoteMessage.getData().get("body"));
    }

    // NOT USED. Always send with data payload
    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent();
            pushNotification.putExtra("message", message); // key : "message" or "text"
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "##### push json: " + json.toString());

        try {
            JSONObject data = json;

            String title = data.getString(getString(R.string.pnotif_data_key01));
            String message = data.getString(getString(R.string.pnotif_data_key02));
//            boolean isBackground = data.getBoolean("is_background");
//            String imageUrl = data.getString("image");
//            String timestamp = data.getString("timestamp");
//            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "##### " + getString(R.string.pnotif_data_key01) + " : " + title);
            Log.e(TAG, "##### " + getString(R.string.pnotif_data_key02) + " : " + message);
//            Log.e(TAG, "isBackground: " + isBackground);
//            Log.e(TAG, "payload: " + payload.toString());
//            Log.e(TAG, "imageUrl: " + imageUrl);
//            Log.e(TAG, "timestamp: " + timestamp);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in FOREGROUND, broadcast the push message
                Intent pushNotification = new Intent(ConfigCustom.PUSH_NOTIF_INTENT);
                pushNotification.putExtra(getString(R.string.pnotif_data_key02), message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {
                // app is in BACKGROUND, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                resultIntent.putExtra(getString(R.string.pnotif_data_key02), message);

                // WITHOUT image attachment
                showNotificationMessage(getApplicationContext(), title, message, "now", resultIntent);

                // WITH image attachment
//                if (TextUtils.isEmpty(imageUrl)) {
//                    showNotificationMessage(getApplicationContext(), title, message, 'now', resultIntent);
//                } else {
//                    // image is present, show notification with image
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
//                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "##### Json Exception: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.e(TAG, "##### Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


    /** ======================================================================================
        =============== NOT USED NOW. TREAT FOLLOWING FUNCTIONS AS REFERENCE =================
        ======================================================================================
     */
    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(com.mdkit.qmed.JobSvc.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        // ======================================= VERSION 1
        // power on
//        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
//        boolean isScreenOn = pm.isScreenOn();
//        if (isScreenOn == false) {
//            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
//            wl.acquire(10000);
//            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
//
//            wl_cpu.acquire(10000);
//        }
//
//        // ======================================= VERSION 2
//        // build notif
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//        RemoteViews contentView = new RemoteViews( getApplicationContext().getPackageName(), R.layout.custom_notification_layout);
//
//        String channelId = getString(R.string.default_notification_channel_id);
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder =
////                new NotificationCompat.Builder(this, channelId)
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle("FCM Message")
//                        .setContentText(messageBody)
//                        .setCustomBigContentView(contentView)
//                        .setAutoCancel(true)
//                        .setSound(defaultSoundUri)
//                        .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        // Since android Oreo notification channel is needed.
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////            NotificationChannel channel = new NotificationChannel(channelId,
////                    "Channel human readable title",
////                    NotificationManager.IMPORTANCE_DEFAULT);
////            notificationManager.createNotificationChannel(channel);
////        }
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        // ======================================= VERSION 3
//        Intent notificationIntent;
//
//        long when = System.currentTimeMillis();
//        int id = (int) System.currentTimeMillis();
//
////        Bitmap bitmap = getBitmapFromURL(image_url);
////        NotificationCompat.BigPictureStyle notifystyle = new NotificationCompat.BigPictureStyle();
////        notifystyle.bigPicture(bitmap);
//        RemoteViews contentView = new RemoteViews( getApplicationContext().getPackageName(), R.layout.custom_notification_layout);
////        contentView.setImageViewBitmap(R.id.image, bitmap);
//        contentView.setTextViewText(R.id.title, "MsgTitle");
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
//                .setSmallIcon(R.mipmap.ic_launcher)
////                .setStyle(notifystyle)
//                .setCustomBigContentView(contentView)
//                .setContentText("MsgBody");
//        NotificationManager mNotificationManager = (NotificationManager) getApplicationContext()
//                .getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
//        notificationIntent.putExtra("single_id",12);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), id, notificationIntent, 0);
//
//        Notification notification = mBuilder.build();
//        notification.contentIntent = contentIntent;
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//        notification.defaults |= Notification.DEFAULT_SOUND;
//        notification.defaults |= Notification.DEFAULT_VIBRATE;
//
//
//        mNotificationManager.notify(0, notification);
    }
}