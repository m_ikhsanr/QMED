/*
 * HISTORY VERSIONING
 * PROGRAMMER
 * INITIAL              VERSION         DATE(YYYY-MM-DD)        CHANGE DESCRIPTION
 * ========================================================================================================
 * CHRIS                1.0             2018-04-27              initial version
 *
 */

package com.mdkit.qmed;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mdkit.qmed.BaseTaskListener.BaseTaskListenerInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

class SendToAppServer extends AsyncTask<Object, Integer, String> {

    private static final String TAG = SendToAppServer.class.getSimpleName();
    private Context context = null;
    private String server_response = null;
    private HttpURLConnection con = null;
    private BaseTaskListenerInterface<String> callback;

    public SendToAppServer(Context context, BaseTaskListenerInterface<String> cb) {
        this.context = context;
        this.callback = cb;
    }

    public SendToAppServer(){

    }

    @Override
    protected void onPostExecute(String result) { // after doInBackground
//        android.os.Debug.waitForDebugger();
        Log.d(TAG, "##### START onPostExecute. result : "+result);
//        String finalResult = result;
////        progressDialog.dismiss();
//        System.out.println("##### onPostExecute called");
//        callback.onTaskComplete(result);
        Log.d(TAG, "##### END onPostExecute. result : "+result);

    }

    @Override
    protected void onPreExecute() { // before doInBackground
        Log.d(TAG, "##### START onPreExecute. ");
        Log.d(TAG, "##### END onPreExecute. ");
    }

    @Override
    protected String doInBackground(Object... args) {
        Log.d(TAG, "##### START doInBackground.");
        HashMap h = (HashMap) args[0];
        if(h.get("rqURL") == null || h.get("rqMethod") == null || h.get("rqContent") == null){
            return null;
        }
        try {
            String requestURL = h.get("rqURL").toString();
            String requestMethod = h.get("rqMethod").toString();
            String requestContent = h.get("rqContent").toString();

            // logic :
            URL url = new URL(requestURL);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(requestMethod);
            if (requestContent.equalsIgnoreCase("json")) {
                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            }else if(requestContent.equalsIgnoreCase("html")){
                con.setRequestProperty("Content-Type", "application/html; charset=UTF-8");
            }
//            con.setDoOutput(true);
            con.setConnectTimeout(20000); // 20 seconds
            con.setReadTimeout(20000); // 20 seconds

            Log.d(TAG, "##### before con.connect().");
            con.connect();
            Log.d(TAG, "##### after con.connect().");

            int responseCode = con.getResponseCode();
            Log.d(TAG, "##### responseCode : "+responseCode);

            // TODO : handle auth failure (in PHP also) : PHP logging, PHP member id get from session
            if(responseCode == HttpURLConnection.HTTP_OK){ // 200
                Log.d(TAG, "##### before readStream. server_response : "+server_response);
                server_response = readStream(con.getInputStream());
                Log.d(TAG, "##### after readStream. server_response : "+server_response);
            }

//            return server_response;
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if (con != null) con.disconnect();
        }
        Log.d(TAG, "##### END doInBackground. server_response : "+server_response);
        return server_response;
    }

    private String readStream(InputStream in) {
        Log.d(TAG, "##### START readStream. ");
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d(TAG, "##### END readStream. response.toString() : "+response.toString());
        return response.toString();
    }
}
