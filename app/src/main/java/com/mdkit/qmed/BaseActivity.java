/*
 * HISTORY VERSIONING
 * PROGRAMMER
 * INITIAL              VERSION         DATE(YYYY-MM-DD)        CHANGE DESCRIPTION
 * ========================================================================================================
 * CHRIS                1.0             2018-04-29              initial version
 *
 */

package com.mdkit.qmed;

import android.Manifest.permission;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    public static final String INFO_SDELIM = "|";

    /** ===================================================================================
     ================= PUBLIC FUNCTIONS (mostly called in MainActivity) ===================
     ======================================================================================
     */
    public HashMap prepHeaderRequest() {
        String allInfo = "", rhs = "", key = "MDKBDG#";

        try {
            String sURL = getString(R.string.appserver_url);
            String prot = getString(R.string.appserver_prot);
            String geheax = getString(R.string.appserver_geheax);

            // 1. prepare body
            allInfo += getLocationInfo(getApplicationContext());
            allInfo += INFO_SDELIM + getDeviceInfo();
//            allInfo += INFO_SDELIM + getIPAddressInfo(true);

            allInfo = URLEncoder.encode(allInfo, "UTF-8");
//            String fbt = readFirebaseToken(); cannot call here coz always return null

            String sNoRhs = prot + "://" + sURL + geheax + "?" + // 'get header exclusive'.php
                    "inf=" + allInfo; // various user infos

            // 2. prepare hash
            rhs = sStringToHMACMD5(key + sNoRhs, key);
            String sWithRhs = sNoRhs + "&rhs=" + rhs; // string with request hash

            // 3. make request
            HashMap rqArgs = new HashMap();
            rqArgs.put("rqURL", sWithRhs);
            rqArgs.put("rqMethod", "GET");
            rqArgs.put("rqContent", "html");

            return rqArgs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void checkAppUpdate(String pMustUpdate, String pVersionName) {
//        String pMustUpdate = rpArr[3].toString(); // from prmeter_tp : Y or N
//        int curVersionCode = BuildConfig.VERSION_CODE; // build.gradle value of versionCode (integer) e.g 8 or 9

//        String pVersionName = rpArr[4].toString(); // from prmeter_tp : version which we want the user to update to. e.g 1.7 or 1.8
        String curVersionName = BuildConfig.VERSION_NAME; // build.gradle value of versionName (string) e.g 1.7 or 1.8

        if (pMustUpdate.toString().trim().equalsIgnoreCase("Y")) { // update is a must
            if (!pVersionName.equalsIgnoreCase(curVersionName)) {
                // show dialog with only 1 choice : UPDATE NOW
                showDialogRequireUpdate(getString(R.string.gplay_url));
            }
        } else { // update is optional
            if (!pVersionName.equalsIgnoreCase(curVersionName)) {
                // show dialog with only 2 choices : UPDATE NOW or LATER
                showDialogRequireUpdate(getString(R.string.gplay_url));
            }
        }
    }

    public String readFirebaseToken() {
        String firebaseToken = "";
        Log.d(TAG, "##### Starting readFirebaseToken() | value : " + firebaseToken);
        SharedPreferences prefs = getSharedPreferences(getString(R.string.sharedpref_name), MODE_PRIVATE);
        firebaseToken = prefs.getString(getString(R.string.sharedpref_firebasetoken), null);
        Log.d(TAG, "##### Ending readFirebaseToken() | value : " + firebaseToken);
        return firebaseToken;
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }

    // called from FirebaseIDSvc
    public static String sStringToHMACMD5(String s, String keyString) {
        String sEncodedString = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacMD5");
            Mac mac = Mac.getInstance("HmacMD5");
            mac.init(key);

            byte[] bytes = mac.doFinal(s.getBytes());

            StringBuffer hash = new StringBuffer();

            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            sEncodedString = hash.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sEncodedString;
    }


    /** ================================================================================
     ======================== PRIVATE FUNCTIONS ========================================
     ===================================================================================
     */

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    private static String getLocationInfo(Context c) {

        double latitude = 0.0, longitude = 0.0;
        String sInfo = "", loclonglatt = "", locaddress = "", loccity = "";

        LocationManager locManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        boolean network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Location location;

        if (network_enabled) {
            if (ActivityCompat.checkSelfPermission(c, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(c, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(location!=null){
                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }
        }

        Geocoder geocoder;
        List<Address> yourAddresses = null;
        geocoder = new Geocoder(c, Locale.getDefault());
        try {
            yourAddresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (yourAddresses.size() > 0) {
                loclonglatt = String.valueOf(latitude) + "," + String.valueOf(longitude);
                locaddress = yourAddresses.get(0).getAddressLine(0); // get Alamat Lengkap
                loccity = yourAddresses.get(0).getSubAdminArea(); // get Kota
            }

//            sInfo = "Long Lat:" + loclonglatt
//            + INFO_SDELIM + "Address:" + locaddress
//            + INFO_SDELIM + "City:" + loccity;
            sInfo = loclonglatt
            + INFO_SDELIM + locaddress
            + INFO_SDELIM + loccity;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return sInfo;
    }

    private static String getDeviceInfo(){
        String sInfo = "";
        try {

//            sInfo = "OS Version:"      + System.getProperty("os.version") + " - " + android.os.Build.VERSION.INCREMENTAL
//            + INFO_SDELIM + "OS API Level:"    + android.os.Build.VERSION.SDK_INT
//            + INFO_SDELIM + "Device:"          + android.os.Build.DEVICE
//            + INFO_SDELIM + "Model - Product:" + android.os.Build.MODEL + " - "+ android.os.Build.PRODUCT
//            + INFO_SDELIM + "RELEASE:"         + android.os.Build.VERSION.RELEASE
//            + INFO_SDELIM + "BRAND:"           + android.os.Build.BRAND
//            + INFO_SDELIM + "DISPLAY:"         + android.os.Build.DISPLAY
//            + INFO_SDELIM + "CPU_ABI:"         + android.os.Build.CPU_ABI
//            + INFO_SDELIM + "CPU_ABI2:"        + android.os.Build.CPU_ABI2
//            + INFO_SDELIM + "UNKNOWN:"         + android.os.Build.UNKNOWN
//            + INFO_SDELIM + "HARDWARE:"        + android.os.Build.HARDWARE
//            + INFO_SDELIM + "Build ID:"        + android.os.Build.ID
//            + INFO_SDELIM + "MANUFACTURER:"    + android.os.Build.MANUFACTURER
//            + INFO_SDELIM + "SERIAL:"          + android.os.Build.SERIAL
//            + INFO_SDELIM + "USER:"            + android.os.Build.USER
//            + INFO_SDELIM + "HOST:"            + android.os.Build.HOST;

            sInfo = System.getProperty("os.version") + " - " + android.os.Build.VERSION.INCREMENTAL
            + INFO_SDELIM + android.os.Build.VERSION.SDK_INT
            + INFO_SDELIM + android.os.Build.DEVICE
            + INFO_SDELIM + android.os.Build.MODEL + " - "+ android.os.Build.PRODUCT
            + INFO_SDELIM + android.os.Build.VERSION.RELEASE
            + INFO_SDELIM + android.os.Build.BRAND
            + INFO_SDELIM + android.os.Build.DISPLAY
            + INFO_SDELIM + android.os.Build.CPU_ABI
            + INFO_SDELIM + android.os.Build.CPU_ABI2
            + INFO_SDELIM + android.os.Build.UNKNOWN
            + INFO_SDELIM + android.os.Build.HARDWARE
            + INFO_SDELIM + android.os.Build.ID
            + INFO_SDELIM + android.os.Build.MANUFACTURER
            + INFO_SDELIM + android.os.Build.SERIAL
            + INFO_SDELIM + android.os.Build.USER
            + INFO_SDELIM + android.os.Build.HOST;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sInfo;
    }

    private static String getIPAddressInfo(boolean useIPv4) { // local ip, NOT public ip
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void showDialogRequireUpdate(final String updateUrl) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.appupd_title))
                .setMessage(getString(R.string.appupd_content))
                .setPositiveButton(getString(R.string.appupd_yes),null)
                .setNegativeButton(getString(R.string.appupd_no),null)
//                .setPositiveButton("Update",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                redirectStore(updateUrl);
//                            }
//                        })
//                .setNegativeButton("No, thanks",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
//                            }
//                        })
                .create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "##### START showDialogRequireUpdate positiveBtnOnClick");
                        redirectStore(updateUrl);
                        Log.d(TAG, "##### END showDialogRequireUpdate positiveBtnOnClick");
                    }
                });

                Button negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "##### START showDialogRequireUpdate negBtnOnClick");
                        Toast.makeText(getApplicationContext(), getString(R.string.appupd_no_msg), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        Log.d(TAG, "##### END showDialogRequireUpdate negBtnOnClick");
                    }
                });
            }
        });

        alertDialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


//    public void getLocationInfo(final String myurl) {
//        LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        boolean network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        Location location;
//
//        if(network_enabled){
//            location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            if(location!=null){
//                longitude = location.getLongitude();
//                latitude = location.getLatitude();
//            }
//        }
//
//        Geocoder geocoder;
//        List<Address> yourAddresses = null;
//        geocoder = new Geocoder(this, Locale.getDefault());
//        try {
//            yourAddresses = geocoder.getFromLocation(latitude, longitude, 1);
//            if (yourAddresses.size() > 0) {
//                loclonglatt = String.valueOf(latitude) + "," + String.valueOf(longitude);
//                locaddress = yourAddresses.get(0).getAddressLine(0); // get Alamat Lengkap
//                loccity = yourAddresses.get(0).getSubAdminArea(); // get Kota
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        insertLocationInfo(loclonglatt, locaddress, loccity, myurl);
//    }
//
//    public void insertLocationInfo(final String loc_longlatt, final String loc_address, final String loc_city, final String url){
//        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
//            @Override
//            protected String doInBackground(String... params) {
//                String loc_longlatt = loclonglatt ;
//                String loc_address = locaddress ;
//                String loc_city = loccity ;
//
//                List<NameValuePair> nameValuePairs = new ArrayList<>();
//
//                nameValuePairs.add(new BasicNameValuePair("loc_longlatt", loc_longlatt));
//                nameValuePairs.add(new BasicNameValuePair("loc_address", loc_address));
//                nameValuePairs.add(new BasicNameValuePair("loc_city", loc_city));
//
//                try {
//                    HttpClient httpClient = new DefaultHttpClient();
//                    HttpPost httpPost = new HttpPost(url);
//                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//                    HttpResponse httpResponse = httpClient.execute(httpPost);
//                    HttpEntity httpEntity = httpResponse.getEntity();
//
//                } catch (ClientProtocolException e) {
//
//                } catch (IOException e) {
//
//                }
//                return "Data Inserted Successfully";
//            }
//
//            @Override
//            protected void onPostExecute(String result) {
//                super.onPostExecute(result);
//            }
//        }
//
//        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
//        sendPostReqAsyncTask.execute(loc_longlatt, loc_address, loc_city);
//    }


}
