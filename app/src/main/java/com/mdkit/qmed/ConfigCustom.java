/*
 * HISTORY VERSIONING
 * PROGRAMMER
 * INITIAL              VERSION         DATE(YYYY-MM-DD)        CHANGE DESCRIPTION
 * ========================================================================================================
 * CHRIS                1.0             2018-04-27              initial version
 *
 */

package com.mdkit.qmed;


public class ConfigCustom {
    public static final String PUSH_NOTIF_INTENT = "pushNotificationIntent";
}
