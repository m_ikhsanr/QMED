/*
 * HISTORY VERSIONING
 * PROGRAMMER
 * INITIAL              VERSION         DATE(YYYY-MM-DD)        CHANGE DESCRIPTION
 * ========================================================================================================
 * CHRIS                1.0             2018-04-27              initial version
 *
 */

package com.mdkit.qmed;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;


public class FirebaseIDSvc extends FirebaseInstanceIdService {

    private static final String TAG = FirebaseIDSvc.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "##### Refreshed token: " + refreshedToken);

        // save to file in sharedpreference
        writeTokenToFile(refreshedToken);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Implement this method to send token to your app server.
        String rhs = "", mid = "", key = "MDKBDG#";
        String sURL = getString(R.string.appserver_url);
        String prot = getString(R.string.appserver_prot);
        String indid = getString(R.string.appserver_indid);

//https://stackoverflow.com/questions/19167954/use-uri-builder-in-android-or-create-url-with-variables
//        Uri.Builder b1 = new Uri.Builder();
//        b1.scheme("http")
//                .authority(sURL)
//                .appendQueryParameter("mid", mid)
//                .appendQueryParameter("did", token)
//        ;
//        String sb1 = b1.build().toString();

        String sNoRhs = prot + "://" + sURL + indid + "?" + // "insert device id".php
                "mid=" + mid + // member id
                "&did=" + token; // device id

        rhs = BaseActivity.sStringToHMACMD5(key + sNoRhs, key); // request hash (bisa juga kepanjangan dari 'rahasia') :D

        String sWithRhs = sNoRhs + "&rhs=" + rhs; // string without request hash
        HashMap rqArgs = new HashMap();
        rqArgs.put("rqURL",sWithRhs);
        rqArgs.put("rqMethod","GET");
        rqArgs.put("rqContent","html");

        Log.d(TAG, "##### START async SendToAppServer...");
        SendToAppServer req = new SendToAppServer();
//            req.execute(rqArgs);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            req.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, rqArgs);
        }else {
            req.execute(rqArgs);
        }
        Log.d(TAG, "##### END async SendToAppServer.");
    }

    // https://stackoverflow.com/questions/23024831/android-shared-preferences-example
    public void writeTokenToFile(String token) {
        try {
            SharedPreferences pref = getApplicationContext().getSharedPreferences( getString(R.string.sharedpref_name), MODE_PRIVATE);

            SharedPreferences.Editor editor = pref.edit();
            editor.putString(getString(R.string.sharedpref_firebasetoken), token);
//        editor.putInt("key_name", 90); // this line is to demonstrate another possible value
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* to check the file, do this :
            1. go to cmd
            2. adb shell
            3. run-as <package-name> (e.g com.mdkit.qmed)
            4. cd shared_prefs
            5. cat com.mdkit.qmed.xml
         */
    }
}