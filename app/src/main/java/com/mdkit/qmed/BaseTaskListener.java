package com.mdkit.qmed;

class BaseTaskListener<T>{
    interface BaseTaskListenerInterface<T> {
        public void onTaskComplete(T result);
    }
}

